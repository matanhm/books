<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'Nir Ohayon',
                        'email' => 'nir0910@gmail.com',
                        'password' => '12345678',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'name' => 'Akiva Jacobs',
                        'email' => 'akivaja@gmail.com',
                        'password' => '87654321',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
            
                    ]);
    }
}
