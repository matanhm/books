@extends('layouts.app')

@section('content')
<h1>Edit Book</h1>
<form method = 'post' action="{{action('BookController@update', $book->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Book to Update:</label>
    <br>
    <a>Book Name: </a>
    <input type= "text" class = "form-control" name= "title" value = "{{$book->title}}">
    <br>
    <a>Book Author: </a>
    <input type= "text" class = "form-control" name= "author" value = "{{$book->author}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Save">
</div>

</form>

<form method = 'post' action="{{action('BookController@destroy', $book->id)}}">
@csrf
@method('DELETE')
<div clas   s = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete Book">
</div>

</form>
@endsection